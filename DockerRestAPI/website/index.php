<html>
    <head>
        <title>CIS 322 REST-api demo:</title>
    </head>

    <body>
        <h3>List all Brevet times</h3>
        <ul>
            <?php
            $json = file_get_contents('http://brevet:5000/listAll');
            $obj = json_decode($json);
	          $times = $obj->result;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h3> List Brevet open times</h3>
        <ul>
            <?php
            $json = file_get_contents('http://brevet:5000/listOpenOnly');
            $obj = json_decode($json);
	          $times = $obj->result;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h3> List Brevet close times</h3>
        <ul>
            <?php
            $json = file_get_contents('http://brevet:5000/listCloseOnly');
            $obj = json_decode($json);
                  $times = $obj->result;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h3> List all Brevet times (csv)</h3>
        <ul>
            <?php
            $json = file_get_contents('http://brevet:5000/listAll/csv');
            $temp = str_split($json);
            foreach ($temp as $l) {
             if ($l == "\\") {
                echo "<br>";}
             else {
                echo $l;}}
            ?>
        </ul>
        <h3> List all Open Brevet times (csv)</h3>
          <ul>
              <?php 
              $json = file_get_contents('http://brevet:5000/listOpenOnly/csv');
              $temp = str_split($json);
              foreach ($temp as $l) {
               if ($l == "\\") {
                 echo "<br>"; } 
               else {
                 echo $l; }}
              ?>
          </ul>
         <h3> List all Close Brevet times (csv)</h3>
            <ul>
                <?php 
                $json = file_get_contents('http://brevet:5000/listCloseOnly/csv');
                $temp = str_split($json);
                foreach ($temp as $l) {
                 if ($l == "\\") {
                   echo "<br>"; } 
                 else {
                   echo $l; }}
                ?>
            </ul> 
    </body>
</html>
