# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Functionality 

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* You will design RESTful service to expose what is stored in MongoDB. Specifically, you'll use the boilerplate given in DockerRestAPI folder, and create the following three basic APIs:
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* You will also design two different representations: one in csv and one in json. For the above, JSON should be your default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* You will also add a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format


## Tasks Completed

The parts that this software does is as follows.

* It exposes all of the start and close times stored in mongo in json and csv format.

* It exposes all of the start only and close only times stored in mongo in json and csv format.

* It exposes both start and close times together in json and csv format.

* It exposes any and all times requested with an arbitrary "k" given.

* Consumer side: It does grab everything from the api section (excluding the top k ones) and displays it all onto one page.


## Issue(s)


I was having techincal issues on Friday, and was having trouble firing up my Mongo container which drained around 5 hours of my time. So, I am submitting this hoping that it works on your side, since I still cannot find the issue (ended up downloading Docker and starting the project over on my old laptop) on my current computer. (Hopefully this is taken as a woe is me, just trying to explain why I wasn't able to produce my typical work). That being said, the issue with this program is that it doesn't display all of the times with the given "k". Also, in regards to printing all of the times via csv format, there is a "n" at the beginning of each line. Couldn't figure out that in time. Also, the consumer display essentially throws everything onto one page. Kind of ugly, but I was able to grab what I needed from the api side. Just needed more time on displaying it in a "prettier" way. 



# Contributor
-------------

Ronny Fuentes <ronnyf@uoregon.edu>
